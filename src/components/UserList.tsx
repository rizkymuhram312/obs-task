import {
  Button,
  Grid,
  Typography,
  Avatar,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  IconButton,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from "@mui/material";
import { useContext, useState } from "react";
import { UsersContext } from "../contexts/UsersContext";
import React from "react";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import CloseIcon from "@mui/icons-material/Close";

export default function UserList() {
  const users = useContext(UsersContext);
  const [userDetail, setUserDetail] = useState<any>();
  const [open, setOpen] = useState(false);

  const getDetail = (id: any) => {
    fetch(`https://jsonplaceholder.typicode.com/users/${id}`)
      .then((res) => res.json())
      .then((data) => {
        // Do something with the user detail data
        setUserDetail(data);
      })
      .catch((error) => {
        console.error("Error fetching user detail:", error);
      });
  };

  const handleDetail = (v: any) => {
    if (v) {
      setOpen(true);
      getDetail(v);
    }
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      <div>
        <Grid container direction="row" justifyContent="space-between">
          <div>
            <h2>User Data</h2>
          </div>
          <div>
            <Button variant="contained" sx={{ m: 2 }}>
              Create
            </Button>
          </div>
        </Grid>
        <div>
          <Grid
            container
            spacing={{ xs: 2, md: 3 }}
            columns={{ xs: 4, sm: 8, md: 12 }}
          >
            {users.map((v, index) => (
              <Grid item xs={6} sm={4} md={4} key={index}>
                <List
                  sx={{
                    width: "100%",
                    maxWidth: 360,
                    bgcolor: "background.paper",
                  }}
                >
                  <ListItem
                    onClick={() => handleDetail(v.id)}
                    alignItems="flex-start"
                    secondaryAction={
                      <React.Fragment>
                        <IconButton edge="end" aria-label="delete">
                          <DeleteIcon />
                        </IconButton>
                        <IconButton edge="end" aria-label="edit">
                          <EditIcon />
                        </IconButton>
                      </React.Fragment>
                    }
                  >
                    <ListItemAvatar>
                      <Avatar
                        alt="photo"
                        src={`https://picsum.photos/id/${v.id}/200`}
                      />
                    </ListItemAvatar>
                    <ListItemText
                      primary={v.name}
                      secondary={
                        <React.Fragment>
                          <Typography
                            sx={{ display: "inline" }}
                            component="span"
                            variant="body2"
                            color="text.primary"
                            marginRight={2}
                          >
                            {v.email}
                          </Typography>
                        </React.Fragment>
                      }
                    />
                  </ListItem>
                </List>
              </Grid>
            ))}
          </Grid>
        </div>
      </div>

      {/* Dialog */}

      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <DialogTitle sx={{ m: 0, p: 2 }} id="customized-dialog-title">
          Detail User
        </DialogTitle>
        <IconButton
          aria-label="close"
          onClick={handleClose}
          sx={{
            position: "absolute",
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
        <DialogContent dividers>
          <p>Name : {userDetail?.name}</p>
          <p>Username : {userDetail?.username}</p>
          <p>Email : {userDetail?.name}</p>
          <p>
            Adress : {userDetail?.address?.street}, {userDetail?.address?.suite}
            , {userDetail?.address?.city}
          </p>
          <p>Phone : {userDetail?.phone}</p>
          <p>Website : {userDetail?.website}</p>
          <p>Company : {userDetail?.company?.name}</p>
        </DialogContent>
      </Dialog>
    </>
  );
}
