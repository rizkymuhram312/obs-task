import React, { createContext, useEffect, useState } from "react";
import { UserInterface } from "../types/model/user";

interface UserContextProviderProps {
    children: React.ReactNode; // or JSX.Element if you're only expecting elements
  }

export const UsersContext = createContext<UserInterface[]>([]);

const UserContextProvider: React.FC<UserContextProviderProps> = ({ children }) => {
  const [users, setUsers] = useState<UserInterface[]>([]);

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/users")
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        setUsers(data);
        console.log(users);
      })
      .catch((error) => {
        console.error("Error fetching users:", error);
      });
  }, []); 
  
  return (
    <UsersContext.Provider value={users}>
      {children}
    </UsersContext.Provider>
  );
};

export default UserContextProvider;
