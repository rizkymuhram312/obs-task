import "./App.css";
import { Container } from "@mui/material";
import UserList from "./components/UserList";
import UserContextProvider from "./contexts/UsersContext";

function App() {
  return (
    <Container maxWidth="lg">
      <UserContextProvider>
        <UserList />
      </UserContextProvider>
    </Container>
  );
}

export default App;
